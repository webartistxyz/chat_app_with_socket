require("./bootstrap");

window.Vue = require("vue");

import VueSocketio from "vue-socket.io";
Vue.use(VueSocketio, "http://localhost:5000");

import InfiniteLoading from "vue-infinite-loading";
Vue.use(InfiniteLoading, {
    /* options */
});

//full page loader
import loading from "vue-full-loading";
Vue.component("loading", loading);

Vue.component("Louncher", require("./components/Louncher.vue"));

const app = new Vue({
    el: "#app",
    data: {
        selected: 0,
        userId: 0,
        activeUser: 0,
        activeUserDetail: "",
        opositeActiveUser: 0,
        opositeUserSelectedContact: 0,
        allOnlineUsers: null,
        allMszs: null,
        allContacts: null,
        spinning: false,
        users: true,
        activeUsersIcon: true,
        searchIcon: true,
        usersIcon: false
    }
});
