<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactsController extends Controller
{
    public function get()
    {
        $fromIds = [];
        $toIds = [];
        $allUniqueIds = [];
        $allUniqueIdWithTime = [];
        $latestMessages = [];

        Message::where('to', auth()->id())->where('read', 0)->update(['get_msz' => true]);

        $messageFrom = Message::where('to', '=', auth()->id())->orderBy('created_at', 'DESC')->get()->unique('from');
        foreach ($messageFrom as $msz) {
            $fromArray = [
                'to' => $msz->from,
                'created_at' => $msz->created_at->timestamp,
            ];
            array_push($fromIds, $fromArray);
        }

        $messageTo = Message::where('from', '=', auth()->id())->orderBy('created_at', 'DESC')->get()->unique('to');
        foreach ($messageTo as $msz) {
            $toArray = [
                'to' => $msz->to,
                'created_at' => $msz->created_at->timestamp,
            ];
            array_push($toIds, $toArray);
        }

        $allIds = array_merge($fromIds, $toIds);

        usort($allIds, function ($a, $b) {
            return $b['created_at'] <=> $a['created_at'];
        });
        $tempArr = array_unique(array_column($allIds, 'to'));
        $allUniqueIdWithTime = array_intersect_key($allIds, $tempArr);

        foreach ($allUniqueIdWithTime as $id) {
            array_push($allUniqueIds, $id['to']);

            $arr = [
                'to' => $id['to'],
                'created_at' => $id['created_at'],
            ];

            array_push($latestMessages, $arr);
        }
        $collecton = collect($latestMessages);
        // get all users except the authenticated one
        $contacts = User::where('id', '!=', auth()->id())->whereIn('id', $allUniqueIds)->orderByRaw(DB::raw("FIELD(id, " . implode($allUniqueIds, ',') . ")"))->paginate(10);
        //$contacts = User::where('id', '!=', auth()->id())->whereNotIn('id', $fromIds)->get();

        // get a collection of items where sender_id is the user who sent us a message
        // and messages_count is the number of unread messages we have from him
        $unreadIds = Message::select(\DB::raw('`from` as sender_id, count(`from`) as messages_count'))
            ->where('to', auth()->id())
            ->where('read', false)
            ->groupBy('from')
            ->get();

        // add an unread key to each contact with the count of unread messages
        $contacts->data = $contacts->transform(function ($contact) use ($unreadIds, $collecton) {
            $contactUnread = $unreadIds->where('sender_id', $contact->id)->first();
            $msgTo = $collecton->where('to', $contact->id)->first();
            $contact->latest = !empty($msgTo['created_at']) ? $msgTo['created_at'] : 0;
            $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;
            return $contact;
        });

        return response()->json($contacts);
    }

    public function getOtherContacts()
    {

        $fromIds = [];
        $toIds = [];
        $allUniqueIds = [];
        $allUniqueIdWithTime = [];
        $latestMessages = [];

        Message::where('to', auth()->id())->where('read', 0)->update(['get_msz' => true]);

        //$messageTo = Message::distinct()->select('from')->where('to', '=', auth()->id())->orderBy('created_at', 'DESC')->groupBy('from')->get();
        $messageFrom = Message::where('to', '=', auth()->id())->orderBy('created_at', 'DESC')->get()->unique('from');
        foreach ($messageFrom as $msz) {
            $fromArray = [
                'to' => $msz->from,
                'created_at' => $msz->created_at->timestamp,
            ];
            array_push($fromIds, $fromArray);
        }

        $messageTo = Message::where('from', '=', auth()->id())->orderBy('created_at', 'DESC')->get()->unique('to');
        foreach ($messageTo as $msz) {
            $toArray = [
                'to' => $msz->to,
                'created_at' => $msz->created_at->timestamp,
            ];
            array_push($toIds, $toArray);
        }

        $allIds = array_merge($fromIds, $toIds);

        usort($allIds, function ($a, $b) {
            return $b['created_at'] <=> $a['created_at'];
        });
        $tempArr = array_unique(array_column($allIds, 'to'));
        $allUniqueIdWithTime = array_intersect_key($allIds, $tempArr);

        foreach ($allUniqueIdWithTime as $id) {
            array_push($allUniqueIds, $id['to']);

            $arr = [
                'to' => $id['to'],
                'created_at' => $id['created_at'],
            ];

            array_push($latestMessages, $arr);
        }
        $collecton = collect($latestMessages);
        // get all users except the authenticated one
        $contacts = User::where('id', '!=', auth()->id())->whereNotIn('id', $allUniqueIds)->paginate(10);

        // get a collection of items where sender_id is the user who sent us a message
        // and messages_count is the number of unread messages we have from him
        $unreadIds = Message::select(\DB::raw('`from` as sender_id, count(`from`) as messages_count'))
            ->where('to', auth()->id())
            ->where('read', false)
            ->groupBy('from')
            ->get();

        // add an unread key to each contact with the count of unread messages
        $contacts->data = $contacts->transform(function ($contact) use ($unreadIds, $collecton) {
            $contactUnread = $unreadIds->where('sender_id', $contact->id)->first();
            $msgTo = $collecton->where('to', $contact->id)->first();
            $contact->latest = !empty($msgTo['created_at']) ? $msgTo['created_at'] : 0;
            $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;
            return $contact;
        });

        return response()->json($contacts);

    }

    public function getMessagesFor($id)
    {
        $messages = Message::where(function ($q) use ($id) {
            $q->where('from', auth()->id());
            $q->where('to', $id);
        })->orWhere(function ($q) use ($id) {
            $q->where('from', $id);
            $q->where('to', auth()->id());
        })
            ->orderBy('created_at', 'DESC')
            ->paginate(12);
        return response()->json($messages);
    }
    public function send(Request $request)
    {
        $message = Message::create([
            'from' => auth()->id(),
            'to' => $request->contact_id,
            'text' => $request->text,
        ]);
        return response()->json($message);
    }
    public function msgSeen(Request $request)
    {
        Message::where('from', $request->msg_seen)->where('to', auth()->id())->update(['read' => true, 'get_msz' => 2]);
    }
    public function get_msg(Request $request)
    {
        Message::where('from', $request->get_msg)->where('to', auth()->id())->update(['get_msz' => true]);

    }
    public function searchUsers(Request $request)
    {
        $data = $request->serch;
        $users = DB::table('users')
            ->where('id', '!=', auth()->id())
            ->where('name', 'LIKE', "%{$data}%")
            ->orWhere('email', 'LIKE', "%{$data}%")
            ->get();
        return $users;
    }

}
